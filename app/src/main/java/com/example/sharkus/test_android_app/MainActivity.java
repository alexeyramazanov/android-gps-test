package com.example.sharkus.test_android_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.location.Location;
import android.view.View;
import android.widget.TextView;

import com.yayandroid.locationmanager.LocationManager;
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.configuration.PermissionConfiguration;
import com.yayandroid.locationmanager.constants.FailType;
import com.yayandroid.locationmanager.constants.ProcessType;
import com.yayandroid.locationmanager.constants.ProviderType;
import com.yayandroid.locationmanager.listener.LocationListener;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private LocationManager locationManager;

    private TextView textLat;
    private TextView textLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textLat = findViewById(R.id.lat);
        textLon = findViewById(R.id.lon);

        locationManager = new LocationManager.Builder(getApplicationContext())
                .configuration(getLocationConfiguration())
                .activity(this)
                .notify(this)
                .build();
        LocationManager.enableLog(true);
    }

    @Override
    protected void onDestroy() {
        locationManager.onDestroy();
        super.onDestroy();
    }

    public void start(View view) {
        locationManager.get();
    }

    public LocationConfiguration getLocationConfiguration() {
        return new LocationConfiguration.Builder()
                .keepTracking(true)
                .askForPermission(new PermissionConfiguration.Builder()
                        .build())
                .useDefaultProviders(new DefaultProviderConfiguration.Builder()
                        .requiredTimeInterval(1 * 1000) //1 second
                        .requiredDistanceInterval(0)
                        .acceptableAccuracy(5.0f)
                        .acceptableTimePeriod(5 * 1000) //5 seconds
                        .gpsMessage("Turn on GPS?")
                        .setWaitPeriod(ProviderType.GPS, 20 * 1000) //20 seconds
                        .setWaitPeriod(ProviderType.NETWORK, 20 * 1000) //20 seconds
                        .build())
                .build();
    }

    @Override
    public void onLocationFailed(@FailType int failType) {
        textLat.setText("Whoops :(");
        textLon.setText("Whoops :(");
    }

    @Override
    public void onLocationChanged(Location location) {
        textLat.setText(String.valueOf(location.getLatitude()));
        textLon.setText(String.valueOf(location.getLongitude()));
    }

    @Override
    public void onProcessTypeChanged(@ProcessType int processType) {
    }

    @Override
    public void onPermissionGranted(boolean alreadyHadPermission) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
